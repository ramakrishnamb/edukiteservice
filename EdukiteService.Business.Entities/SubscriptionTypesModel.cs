﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EdukiteService.Business.Entities
{
    public class SubscriptionTypesModel
    {
        public int SubscriptionTypeId { get; set; }
        public string SubscriptionType { get; set; }
    }
}
