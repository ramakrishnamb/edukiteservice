﻿using EdukiteService.Business.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EdukiteService.Business.Entities
{
    public class NotificationsModel
    {
        public int Id { get; set; }
        public int TypeId { get; set; }
        public string Message { get; set; }
        public int TopicId { get; set; }
        public DateTime DateTime { get; set; }
        public bool IsRead { get; set; }
        
    }
    public class NotificationRespose : JsonResponse {
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int? Count { get; set; }
    }
}
