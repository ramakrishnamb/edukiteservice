﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EdukiteService.Business.Entities
{
    public class ExamPaperSubQuestionOptionModel
    {
        public int ExamPaperSubQuestionOptionId { get; set; }
        public string OptionNumber { get; set; }
        public Nullable<int> OptionOrder { get; set; }
        public string OptionText { get; set; }
        public Nullable<int> ExamPaperSubQuestionId { get; set; }
    }
}
