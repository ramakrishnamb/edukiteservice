﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EdukiteService.Business.Entities
{
    public class ExamPaperSubQuestionModel
    {
        public int ExamPaperSubQuestionId { get; set; }
        public Nullable<int> QuestionNo { get; set; }
        public string QuestionText { get; set; }
        public string Note { get; set; }
        public Nullable<int> EndsAt { get; set; }
        public Nullable<int> StartsAt { get; set; }
        public Nullable<int> Marks { get; set; }
        public Nullable<int> ExamPaperQuestionId { get; set; }
    }
}
