﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EdukiteService.Business.Entities
{
   public class CommonSubTopicAssetModel
    {
        [Key]
        public IEnumerable<SubTopicModel> SubTopicVM { get; set; }
        public IEnumerable<AssetModel> AssetVM { get; set; }
    }
}
