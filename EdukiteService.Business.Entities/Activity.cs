﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EdukiteService.Business.Entities
{
    public abstract class Activity
    {
        public int HighActive { get; set; }
        public int LowActive { get; set; }
        public int InActive { get; set; }

        public int MainAuthorityAvg { get; set; } = 0;
        public int SubAuthorityAvg { get; set; } = 0;

    }

    public class ProgressReportsProperty
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int AssetsRecommended { get; set; } = 0;
        public int AssetsCovered { get; set; } = 0;


    }
}
