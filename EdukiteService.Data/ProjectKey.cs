//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EdukiteService.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class ProjectKey
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ProjectKey()
        {
            this.ExtendedProjectKeys = new HashSet<ExtendedProjectKey>();
            this.InstallationDetails = new HashSet<InstallationDetail>();
        }
    
        public int ProjectKeyId { get; set; }
        public Nullable<int> ProjectDetailId { get; set; }
        public Nullable<int> Duration { get; set; }
        public Nullable<System.DateTime> ExpiryDate { get; set; }
        public Nullable<int> LicenseKeyTypeId { get; set; }
        public string Key { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ExtendedProjectKey> ExtendedProjectKeys { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<InstallationDetail> InstallationDetails { get; set; }
        public virtual LicenseKeyType LicenseKeyType { get; set; }
        public virtual ProjectDetail ProjectDetail { get; set; }
    }
}
