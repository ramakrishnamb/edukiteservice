﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Messaging;
using System.Configuration;
using EdukiteService.Utilities;

namespace EdukiteService.Utilities
{
    public sealed class Common
    {
        private static Common _instance;

        private Common() { }

        public static Common Instance
        {
            get { return _instance ?? (_instance = new Common()); }
        }

        public static bool ValidateNumber(string input)
        {
            try
            {
                Regex regex = new Regex(@"^\d{9}$");
                if (regex.IsMatch(input))
                    return true;
                return false;
            }
            catch (Exception)
            {
                return false;
            }

        }
        
        
        public static bool IsJson(string input)
        {
            input = input.Trim();
            return input.StartsWith("{") && input.EndsWith("}")
                   || input.StartsWith("[") && input.EndsWith("]");
        }

        public static string RandomPassword(int length)
        {
            const string validCharacters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            StringBuilder randomPassword = new StringBuilder();
            Random random = new Random();

            while (0 < length--)
            {
                randomPassword.Append(validCharacters[random.Next(validCharacters.Length)]);
            }
            return randomPassword.ToString();
        }

        public static int ConvertTimeSpanToMinutes(TimeSpan timeSpan)
        {
            return ((timeSpan.Hours * 60) + timeSpan.Minutes);
        }

        public static string FormatName(string firstName, string middleName, string lastName)
        {
            var nameString = Regex.Replace(string.Concat(firstName, " ", middleName, " ", lastName), @"\s{2,}", " ");
            return nameString.Trim();
        }

        public static string FormatDate(DateTime date)
        {
            return date.ToString("yyyy-MM-dd");
        }

        public static bool ValidateDateTime(string value)
        {
            DateTime date;

            return DateTime.TryParseExact(value, Constants.DATEFORMAT, CultureInfo.InvariantCulture, DateTimeStyles.None, out date);

        }

        public static MessageQueue GetRemoteMSMQ()
        {
            MessageQueue MyQueue;
            if (MessageQueue.Exists(ConfigurationManager.AppSettings["RemotePrivateQueueName"].ToString()))
            {
                MyQueue = new MessageQueue(ConfigurationManager.AppSettings["RemotePrivateQueueName"].ToString());
            }
            else
            {
                MyQueue = MessageQueue.Create(ConfigurationManager.AppSettings["RemotePrivateQueueName"].ToString());
            }

            return MyQueue;
        }
        public static MessageQueue GetClientMSMQ()
        {
            MessageQueue MyQueue;
            if (MessageQueue.Exists(ConfigurationManager.AppSettings["ClientPrivateQueueName"].ToString()))
            {
                MyQueue = new MessageQueue(ConfigurationManager.AppSettings["ClientPrivateQueueName"].ToString());
            }
            else
            {
                MyQueue = MessageQueue.Create(ConfigurationManager.AppSettings["ClientPrivateQueueName"].ToString());
            }

            return MyQueue;
        }

        public static MessageQueue GetMSMQ()
        {
            MessageQueue MyQueue;
            if (MessageQueue.Exists(ConfigurationManager.AppSettings["PrivateQueueName"].ToString()))
            {
                MyQueue = new MessageQueue(ConfigurationManager.AppSettings["PrivateQueueName"].ToString());
            }
            else
            {
                MyQueue = MessageQueue.Create(ConfigurationManager.AppSettings["PrivateQueueName"].ToString());
            }

            return MyQueue;
        }

        public static MessageQueue GetScheduledMSMQ()
        {
            MessageQueue MyQueue;
            if (MessageQueue.Exists(ConfigurationManager.AppSettings["ScheduledPrivateQueueName"].ToString()))
            {
                MyQueue = new MessageQueue(ConfigurationManager.AppSettings["ScheduledPrivateQueueName"].ToString());
            }
            else
            {
                MyQueue = MessageQueue.Create(ConfigurationManager.AppSettings["ScheduledPrivateQueueName"].ToString());
            }

            return MyQueue;
        }
    }
}
