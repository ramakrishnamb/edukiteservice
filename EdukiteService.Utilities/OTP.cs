﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace EdukiteService.Utilities
{
    public sealed class OTP
    {
        private static OTP _instance;

        private OTP() { }

        public static OTP Instance
        {
            get { return _instance ?? (_instance = new OTP()); }
        }

        public static string GenerateRandomNumber(int length)
        {
            try
            {
                string[] outputArray = Constants.ALLOWEDCHARACTERS.Split(',');
                string randomNumber = string.Empty;
                string temp = string.Empty;
                Random rand = new Random();

                for (int i = 0; i < length; i++)
                {
                    temp = outputArray[rand.Next(0, outputArray.Length)];
                    randomNumber += temp;
                }
                return randomNumber;
            }
            catch (Exception ex)
            {
                //need to write logging
                throw;
            }
        }
        
    }
}
