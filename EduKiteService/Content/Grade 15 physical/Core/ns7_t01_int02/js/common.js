/**
 * Created by sairam on 29/07/15.
 */
var currScreen = 1;
var totalScreens = 0;
var navButtonWidthAndSpace = 70;
var prevClickedBtn = null;// navigation button
var alertOpend  = false;
var setPosions = null;
var showCorrectAnswers = null;
var deptCount = 0;

function addNavigation(setPosionsParam, tScreens){
    totalScreens = tScreens;
    setPosions = setPosionsParam;
    for(i=0; i<(totalScreens+2); i++){
        var left = i*navButtonWidthAndSpace;

        var id = (i!=0 && i!=(totalScreens+1))?"screen_"+i:(i==0?"prevBtn":"nextBtn");
        var label = (i!=0 && i!=(totalScreens+1))?i:(i==0?"<":">");

        $("#navHolder").append("<div id='"+id+"' class='navButton' style='top:0px; left:"+left+"px;' ><h3>"+label+"</h3></div>");
    }

    $("#prevBtn").addClass("button-disable");
    $("#screen_1").addClass("button-disable");
    prevClickedBtn = $("#screen_1");

    $(".navButton").mouseover(function (){
        if($(this).hasClass("button-disable")){
            return;
        }
        $(this).addClass("navButton-over");
    });

    $(".navButton").mousedown(function (){
        if($(this).hasClass("button-disable")){
            return;
        }
        $(this).addClass("navButton-over");
    });
    $(".navButton").mouseout(function (){
        $(this).removeClass("navButton-over");
    });

    $(".navButton").click(function (){
        if(alertOpend){
            return;
        }
        if($(this).hasClass("button-disable")){
            return;
        }
        var clickedId = $(this).attr("id");

        if(clickedId == "prevBtn"){
            currScreen--;
        }else if(clickedId == "nextBtn"){
            currScreen++;
        }else{
            currScreen = Number(clickedId.split("_")[1]);

        }
        updateScreen(currScreen);
        //0alert(clickedId+"    : "+currScreen)

    });

    //Navigation click event ends


}

function updateScreen(screenNum){
    currScreen = screenNum;
    $("#counter").text(screenNum);
    if(screenNum == 1){
        $("#prevBtn").addClass("button-disable");
        $("#nextBtn").removeClass("button-disable");
    }else if(screenNum == totalScreens){
        $("#nextBtn").addClass("button-disable");
        $("#prevBtn").removeClass("button-disable");
    }else{
        $("#prevBtn").removeClass("button-disable");
        $("#nextBtn").removeClass("button-disable");
    }
    $("#screen_"+screenNum).addClass("button-disable");
    if(prevClickedBtn)
        prevClickedBtn.removeClass("button-disable");
    prevClickedBtn = $("#screen_"+screenNum);
    //oprionCircle-correct
    setPosions();
}


var tryAgainCallBack = null;

function addIncorrectAlert(showCorrectAnswersParam, tryAgainFun){
    tryAgainCallBack = tryAgainFun;
    $("#navBar").append("<div id='responceAlert'></div>");
    $("#responceAlert").append("<div id='tryAgainBtn'>Try again</div>");
    $("#responceAlert").append("<div id='showAnsBtn'>Show correct answers</div>");
    $("#responceAlert").hide();

    $("#tryAgainBtn").click(function (){
        alertOpend = false;
        $("#responceAlert").animate({opacity:0, top:100}, function(){
            $("#responceAlert").hide();
        });
        tryAgainCallBack();
    });
    $("#showAnsBtn").click(function (){
        alertOpend = false;
        $("#responceTxt").hide();
        $("#responceAlert").animate({opacity:0, top:100}, function(){
            $("#responceAlert").hide();
        });
        showCorrectAnswers();
    });
    showCorrectAnswers = showCorrectAnswersParam;
}

function addCheckButton(){
    $("#navBar").append("<div id='checkBtn'><h3>Check</h3></div>");
    $("#checkBtn").mouseover(function (){
        if($("#checkBtn").css("opacity") == .5){
            return;
        }
        $("#checkBtn").addClass("checkBtnOver");
    });

    $("#checkBtn").mouseout(function (){
        $("#checkBtn").removeClass("checkBtnOver");
    });
}

function addInstructionsButton(){
    $("#navBar").append("<div id='instBtn'><h3>Instructions</h3></div>");

    $("#instBtn").mouseover(function (){

        $("#instBtn").addClass("instBtn-hover");

    });
    $("#instBtn").mouseout(function (){

        $("#instBtn").removeClass("instBtn-hover");

    });

    $("#instBtn").mousedown(function (){

        $("#instBtn").addClass("instBtn-down");

    });
    $("#instBtn").mouseup(function (){

        $("#instBtn").removeClass("instBtn-down");

    });

    $("#insCloseBtn").click(function (){

        $("#instPanel").hide();

    });
}

function enableCheck(flag){
    if(flag){
        $("#checkBtn").css("opacity","1");
        $("#checkBtn").css("cursor","pointer");
    }else{
        $("#checkBtn").css("opacity",".5");
        $("#checkBtn").css("cursor","default");
    }

}


function randamiseArray(inpArr) {
    var tempArr = [];
    while (inpArr.length > 0) {
        var randomIndex = Math.floor(Math.random()*inpArr.length);
        tempArr.push(inpArr[randomIndex]);
        inpArr.splice(randomIndex, 1);
    }
    return tempArr;
}
function addElement(parent, id){
    $("#"+parent).append("<div id='"+id+"'></div>");
    return $("#"+id);
}

function updtaeBackground(ob, url){
    ob.css("background", "url("+url+")no-repeat");
    ob.css("background-size", "100%");
}

