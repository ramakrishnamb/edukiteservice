﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EduKiteService.Configuration
{
    public static class AutoMapperConfig
    {
        private static IEnumerable<Type> GetProfiles()
        {
            var assemblies = AppDomain.CurrentDomain.GetAssemblies().Where(f => f.FullName.Contains("EdukiteService.Bussiness"));
            foreach (var assembly in assemblies)
            {
                try
                {
                    foreach (var type in assembly.GetTypes().Where(type => type.GetConstructors().Any(c => c.GetParameters().Count() == 0)
                         && !type.IsAbstract && typeof(Profile).IsAssignableFrom(type)))
                    {
                        yield return type;
                    }
                }
                finally { }
            }

        }

        public static void Configure()
        {
            Mapper.Initialize(x =>
            {

                var profiles = GetProfiles();
                foreach (var profile in profiles)
                {
                    x.AddProfile(Activator.CreateInstance(profile) as Profile);
                }
            });
        }
    }
}