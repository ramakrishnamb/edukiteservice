﻿using EdukiteService.Utilities;
using EdukiteService.Data;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Net;
using EdukiteService.Business.Entities;

namespace EduKiteService.Models
{
    public class jsonResponse
    {
        public int status { get; set; }
        public string statusMessage { get; set; }
        public object data { get; set; }
    }

    public class JsonResponseAssets
    {
        public int status { get; set; }
        public string statusMessage { get; set; }
        public List<Asset> data { get; set; }
    }

    public class JsonResponseAvatar
    {
        public int status { get; set; }
        public string statusMessage { get; set; }
        public List<ImageMaster> data { get; set; }
    }

    public class JsonResponseSubjects
    {
        public int status { get; set; }
        public string statusMessage { get; set; }
        public List<RespSubject> data { get; set; }
    }

    public class RespSubject
    {
        public int SubjectId { get; set; }
        public string SubjectName { get; set; }
    }

    public class ReqBookMark
    {
        public int userId { get; set; }
        public int deviceId { get; set; }
        public int subjectId { get; set; }
        public int topicId { get; set; }
        public int subTopicId { get; set; }
        public System.Guid assetId { get; set; }
        public string bookmark { get; set; }
    }
    public class BaseApi : ApiController
    {
        private IDictionary<string, object> expandoObject;

        protected Enums Status { private get; set; }

        public BaseApi()
        {
            expandoObject = new ExpandoObject();
        }

        protected HttpResponseMessage Response<T>(Func<T> func)
        {
            try
            {
                //return (JsonResponse)(object)func.Invoke();
                return Request.CreateResponse(HttpStatusCode.OK, func.Invoke());

            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, GenerateResponse(ex));
            }

        }
        protected HttpResponseMessage Response(Action action)
        {
            try
            {
                action.Invoke();
                return Request.CreateResponse(HttpStatusCode.OK, GenerateResponse());
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, GenerateResponse(ex));
            }

        }

        protected JsonResponse GenerateResponse(Exception exception = null)
        {
            var response = new JsonResponse();

            if (exception != null)
            {
                response.Status = Enums.E001.ToString();
                response.StatusMessage = string.Format("{0}:{1}", "Failure", exception.Message);
                return response;
            }

            switch (Status)
            {
                case Enums.E005:
                    {
                        response.Status = Enums.E005.ToString();
                        response.StatusMessage = string.Empty;
                        return response;
                    }
                default:
                    {
                        response.Status = Enums.S001.ToString();
                        response.StatusMessage = "SUCCESS";
                        return response;
                    }
            }
        }


        protected IDictionary<string, object> GenerateResponse<T>(string name, IEnumerable<T> obj)
        {
            expandoObject["statusMessage"] = Enums.S001.ToString();
            expandoObject["status"] = "Success";
            expandoObject[name] = obj;
            return expandoObject;
        }

        //protected IDictionary<string, object> GenerateResponse()
        //{
        //    expandoObject["statusMessage"] = Enums.S001.ToString();
        //    expandoObject["status"] = "Success";
        //    return expandoObject;
        //}

        protected IDictionary<string, object> GenerateResponse(Dictionary<string, object> keyvaluePair)
        {
            expandoObject["statusMessage"] = Enums.S001.ToString();
            expandoObject["status"] = "Success";
            foreach (var items in keyvaluePair)
            {
                expandoObject[items.Key] = items.Value;
            }
            return expandoObject;
        }

        //protected IDictionary<string, object> GenerateResponse(Exception ex)
        //{
        //    expandoObject["statusMessage"] = Enums.E001.ToString();
        //    expandoObject["status"] = string.Format("{0}:{1}", "Failure", ex.Message);
        //    return expandoObject;
        //}

        //    private IDictionary<string, object> createObj<T>(string name, T obj)
        //    {
        //        var returnClass = new ExpandoObject() as IDictionary<string, object>;
        //        PropertyInfo[] props = typeof(T).GetProperties();
        //        foreach (PropertyInfo prop in props)
        //        {
        //            object[] attrs = prop.GetCustomAttributes(true);
        //            if (attrs.Length == 0)
        //            {
        //                returnClass.Add(prop.Name, prop.GetValue(obj));
        //            }
        //        }
        //        return returnClass;
        //    }
    }
}
