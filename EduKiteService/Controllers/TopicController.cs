﻿using EdukiteService.Business.Entities;
using EdukiteService.Utilities;
using EdukiteService.Business.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace EduKiteService.Controllers
{
    public class TopicController : ApiController
    {
        public readonly ITopicService _topicService;
        public TopicController(ITopicService topicService)
        {
            _topicService = topicService;
        }
        [HttpGet]
        [Route("api/Topic/GetTopicDetails")]
        public HttpResponseMessage GetSubjectDetails(TopicDetailsModel tops)
        {

            var provinces = _topicService.GetTopicDetails(tops.topicId);

            return Request.CreateResponse(HttpStatusCode.OK, new APIMessage { StatusCode = (int)HttpStatusCode.OK, StatusMessage = Constants.SuccessMessage, Data = provinces });

        }
    }
}
