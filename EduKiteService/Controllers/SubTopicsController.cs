﻿using EdukiteService.Business.Entities;
using EdukiteService.Business.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace EdukiteService.Controllers
{
    public class SubTopicsController : Controller
    {
        private readonly ISubTopicService _subTopicService;
        private readonly IAssetService _assetService;
        private readonly ITopicService _topicService;
        private readonly ISubjectService _subjectService;
        private readonly IGradeService _gradeService;
        public SubTopicsController(ISubTopicService subTopicService, IAssetService assetService,ITopicService topicService,ISubjectService subjectService, IGradeService gradeService)
        {
            _subTopicService = subTopicService;
            _assetService = assetService;
            _topicService = topicService;
            _subjectService = subjectService;
            _gradeService = gradeService;
        }

        // GET: SubTopics
        public ActionResult GetSubTopics(int id)
        {
            
            ViewBag.TopicId = id;
            CommonSubTopicAssetModel model = new CommonSubTopicAssetModel();
            model.SubTopicVM = _subTopicService.GetSubTopics(id);
            model.AssetVM = _assetService.GetAssetByTopicId(id);

            //TopicId = Convert.ToInt32(assetData.TopicId);
            TopicModel tdata = _topicService.GetTopicById(id);
           int subjectId = tdata.SubjectId;
            SubjectModel sdata = _subjectService.GetSubjectById(subjectId);
            int gradeId = Convert.ToInt32(sdata.GradeID);


            TopicInitialization(gradeId, subjectId, id);

            ViewBag.AssetsWithoutSubTopic = _assetService.GetAssetByTopicIdWithoutSubTopic(id);
            ViewBag.SubTopicAssetData = model;

           

            return View(model);
        }

        private void TopicInitialization(int GradeID, int SubId, int topicId)
        {
            var modelSubject = _subjectService.GetSubjectById(SubId);
            string GradeName = _gradeService .GetGradeNameByGradeID(GradeID);
            ViewData["vdWithoutSubGradeName"] = GradeName;
            ViewData["vdWithoutSubSubjectName"] = modelSubject.SubjectName;
            var modelTopic = _topicService.GetTopicById(topicId);
            ViewData["vdWithoutSubTopicName"] = modelTopic.TopicName;
        }

        [HttpGet]
        public ActionResult Create(int id)

        {


            TempData["TopicId"] = id;
            TempData.Keep();

            TopicModel tdata = _topicService.GetTopicById(id);
            int subjectId = tdata.SubjectId;
            SubjectModel sdata = _subjectService.GetSubjectById(subjectId);
            int gradeId = Convert.ToInt32(sdata.GradeID);
            TopicInitialization(gradeId, subjectId, id);

            CommonSubTopicAssetModel model = new CommonSubTopicAssetModel();
            model.SubTopicVM = _subTopicService.GetSubTopics(id);
            model.AssetVM = _assetService.GetAssetByTopicId(id);
            ViewBag.SubTopicAssetData = model;
            ViewBag.AssetsWithoutSubTopic = _assetService.GetAssetByTopicIdWithoutSubTopic(id);
            return View();
        }

        [HttpPost]
        public ActionResult Create(SubTopicModel subTopicData)
        {
            try
            {
                subTopicData.TopicId = Convert.ToInt32(TempData["TopicId"]); 
                _subTopicService.AddSubTopics(subTopicData);
            }
            catch (Exception ex)
            {

            }
            return RedirectToAction("GetSubTopics", "SubTopics", new { @id = Convert.ToInt32(TempData["TopicId"]) });
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var subTopicData = new SubTopicModel();
            try
            {
                
                CommonSubTopicAssetModel model = new CommonSubTopicAssetModel();              
                var stData = _subTopicService.GetSubTopicById(id);
                int topicId = Convert.ToInt32(stData.TopicId);
                ViewBag.AssetsWithoutSubTopic = _assetService.GetAssetByTopicIdWithoutSubTopic(topicId);
                model.SubTopicVM = _subTopicService.GetSubTopics(topicId);
                model.AssetVM = _assetService.GetAssetByTopicId(topicId);
                ViewBag.SubTopicAssetData = model;

                TopicModel tdata = _topicService.GetTopicById(topicId);
                int subjectId = tdata.SubjectId;
                SubjectModel sdata = _subjectService.GetSubjectById(subjectId);
                int gradeId = Convert.ToInt32(sdata.GradeID);
                TopicInitialization(gradeId, subjectId, topicId);

                if (stData != null)
                {
                    subTopicData.SubtopicId = stData.SubtopicId;
                    subTopicData.SubtopicName = stData.SubtopicName;
                    subTopicData.TextIcon = stData.TextIcon;
                    subTopicData.TopicId = stData.TopicId;
                    subTopicData.Description = stData.Description;
                }
            }
            catch(Exception ex)
            {

            }
            return View(subTopicData);

        }
        [HttpPost]
        public ActionResult Edit(SubTopicModel subTopicData)
        {
            try
            {
                _subTopicService.UpdateSubTopics(subTopicData);
            }
            catch (Exception ex)
            {

            }
            return RedirectToAction("GetSubTopics", "SubTopics",new{@id = subTopicData.TopicId });
        }
        public JsonResult IsSubTopicNameExists(string SubtopicName,string initialSubTopicName)
        {
            if (SubtopicName == initialSubTopicName)
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }

            var data = _subTopicService.GetSubTopicByName(SubtopicName);

            if (data != null)
            {
                return Json("SubTopic name already exists", JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }

        }


        public ActionResult Delete(int subTopicId, int topicId)
        {
            try
            {
                _assetService.DeleteAssetBySubTopicId(subTopicId);
                _subTopicService.DeleteSubTopic(subTopicId);
            }
            catch (Exception ex)
            {

            }

            return RedirectToAction("GetSubTopics", "SubTopics", new { @id = topicId });
        }


        [HttpPost]
        public async Task<ActionResult> UpdateSubTopicsOrderAsync(List<SubTopicModel> lstSubTopicModel)
        {
            string msg = string.Empty;
            try
            {
                msg = await _subTopicService.UpdateSortedSubTopics(lstSubTopicModel);
                if (!string.IsNullOrEmpty(msg))
                {
                    return Json(msg, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(true, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        [HttpPost]
        public async Task<ActionResult> UpdateAssetOrderAsync(List<AssetModel> lstAssetModel)
        {
            string msg = string.Empty;
            try
            {
                msg = await _assetService.UpdateSortedAsset(lstAssetModel);
                if (!string.IsNullOrEmpty(msg))
                {
                    return Json(msg, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(true, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}