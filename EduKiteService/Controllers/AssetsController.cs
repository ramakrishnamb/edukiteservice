﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using EdukiteService.Business;
using System.Data.Entity;
using EdukiteService.Data;

namespace EduKiteService.Controllers
{
    public class AssetsController : ApiController
    {
        Dictionary<string, string> keyValuePairs;

        [HttpPost]
        [ActionName("GetAssets")]
        public Models.JsonResponseAssets GetAssets(JObject jInput)
        {
            Asset ipAsset = JsonConvert.DeserializeObject<Asset>(jInput.ToString());

            try
            {
                using (var context = new Edukite_AndroidEntities())
                {
                    context.Configuration.ProxyCreationEnabled = false;
                    List<Asset> lstAsset = context.Assets.Where(v => v.TopicId == ipAsset.TopicId).ToList();

                    if (lstAsset.Count() > 0)
                    {
                        return new Models.JsonResponseAssets() { status = 200, statusMessage = "Success", data = lstAsset };
                    }
                    else
                    {
                        return new Models.JsonResponseAssets() { status = 400, statusMessage = "Failure" };
                    }
                }
            }
            catch (Exception ex)
            {
                return new Models.JsonResponseAssets() { status = 400, statusMessage = "Failure: " + ex.Message };
            }

        }

        [HttpPost]
        [ActionName("DownloadAsset")]
        public Models.jsonResponse DownloadAsset(JObject jInput)
        {
            User ipUser = JsonConvert.DeserializeObject<User>(jInput.ToString());
            keyValuePairs = new Dictionary<string, string>();

            return new Models.jsonResponse() { status = 200, statusMessage = "Success" };
        }

    }
}
