﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EduKiteService.Controllers
{
    public class SubTopicController : Controller
    {
        // GET: SubTopic
        public ActionResult Index()
        {
            return View();
        }

        // GET: SubTopic/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: SubTopic/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: SubTopic/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: SubTopic/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: SubTopic/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: SubTopic/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: SubTopic/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
