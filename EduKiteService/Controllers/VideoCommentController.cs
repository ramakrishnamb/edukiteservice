﻿using EdukiteService.Utilities;
using EdukiteService.Business.Interface;
using EdukiteService.Business.Entities;
using EduKiteService.Models;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace EduKiteService.Controllers
{
    public class VideoCommentController : BaseApi
    {
        private readonly IVideoCommentService _VideoCommentService;
        public VideoCommentController(IVideoCommentService VideoCommentService)
        {
            _VideoCommentService = VideoCommentService;
        }
        [HttpPost]
        [ActionName("GetComments")]
        public HttpResponseMessage GetComments(JObject videoComment)
        {
            return Response(() =>
            {
                var pageNum = ((JObject)videoComment).GetValue("PageNum", StringComparison.OrdinalIgnoreCase).Value<int>();
                var pageSize = ((JObject)videoComment).GetValue("PageSize", StringComparison.OrdinalIgnoreCase).Value<int>();
                return _VideoCommentService.GetComments(videoComment.ToObject<VideoCommentModel>(), pageNum, pageSize);
            });
        }


        [HttpPost]
        [ActionName("GetRepliesComments")]
        public HttpResponseMessage GetRepliesComments(JObject videoComment)
        {
            return Response(() =>
            {
                var pageNum = ((JObject)videoComment).GetValue("PageNum", StringComparison.OrdinalIgnoreCase).Value<int>();
                var pageSize = ((JObject)videoComment).GetValue("PageSize", StringComparison.OrdinalIgnoreCase).Value<int>();
                return _VideoCommentService.GetRepliesComments(videoComment.ToObject<VideoCommentModel>(), pageNum, pageSize);
            });
        }


        [HttpPost]
        [ActionName("AddComment")]
        public HttpResponseMessage AddComment([FromBody]VideoCommentModel videoComment)
        {
            return Response(() =>
            {
                _VideoCommentService.AddComment(videoComment);
            });

        }

        [HttpPost]
        [ActionName("AddReply")]
        public HttpResponseMessage AddReply([FromBody]VideoCommentModel videoComment)
        {
            return Response(() =>
            {
                if (!_VideoCommentService.AddReply(videoComment))
                {
                    Status = Enums.E005;
                }
            });
        }

    }
}
