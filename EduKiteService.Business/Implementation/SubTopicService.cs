﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EdukiteService.Business.Interface;
using EdukiteService.Data.interfaces;
using EdukiteService.Data;
using EdukiteService.Business.Entities;
using AutoMapper;

namespace EdukiteService.Business.Implementation
{
    public class SubTopicService : ISubTopicService
    {
        public IRepository<Subtopic> _subTopicRepository;
        public IMapper mapper { get; set; }
        public SubTopicService(IRepository<Subtopic> subTopicRepository)
        {
            _subTopicRepository = subTopicRepository;
        }

        public void AddSubTopics(SubTopicModel stData)
        {
            int subtopicNo = _subTopicRepository.Fetch(x => x.TopicId == stData.TopicId).Count();

            if (subtopicNo >= 0)
            {
                var subtopicNotoInsert = 1 + subtopicNo;
                stData.SubtopicNo = subtopicNotoInsert;
            }
            
            Subtopic data = mapper.Map<SubTopicModel, Subtopic>(stData);
            _subTopicRepository.Add(data);
            _subTopicRepository.SaveChanges();
        }

        public List<SubTopicModel> GetSubTopics(int topicId)
        {
            var subTopicData = _subTopicRepository.Fetch(x => x.TopicId == topicId).OrderBy(x => x.SubtopicNo).ToList();
            return mapper.Map<List<Subtopic>, List<SubTopicModel>>(subTopicData);
        }

        public SubTopicModel GetSubTopicByName(string SubtopicName)
        {
            var subTopicData = _subTopicRepository.Fetch(x => x.SubtopicName == SubtopicName).SingleOrDefault();
            return mapper.Map<Subtopic, SubTopicModel>(subTopicData);
        }

        public SubTopicModel GetSubTopicById(int SubtopicId)
        {
            var subTopicData = _subTopicRepository.Fetch(x => x.SubtopicId == SubtopicId).SingleOrDefault();
            return mapper.Map<Subtopic, SubTopicModel>(subTopicData);
        }

        public void UpdateSubTopics(SubTopicModel stData)
        {
            Subtopic data = mapper.Map<SubTopicModel, Subtopic>(stData);
            _subTopicRepository.UpdateChanges(data);
        }

        public void DeleteSubTopic(int SubtopicId)
        {
            Subtopic record = _subTopicRepository.Fetch(x => x.SubtopicId == SubtopicId).SingleOrDefault();
            _subTopicRepository.Delete(record);
            _subTopicRepository.SaveChanges();
        }

        public int AddSubTopicRetID(SubTopicModel subTopicModel)
        {

            var record = _subTopicRepository.Fetch(x => x.SubtopicName == subTopicModel.SubtopicName && x.TopicId == subTopicModel.TopicId).SingleOrDefault();
            SubTopicModel clientModel = mapper.Map<Subtopic, SubTopicModel>(record);

            if (clientModel != null && clientModel.SubtopicId != 0)
            {
                return clientModel.SubtopicId;
            }
            else
            {
                Subtopic data = mapper.Map<SubTopicModel, Subtopic>(subTopicModel);
                _subTopicRepository.Add(data);
                _subTopicRepository.SaveChanges();

                return data.SubtopicId;
            }

        }

        public async Task<string> UpdateSortedSubTopics(List<SubTopicModel> lstSubTopicModel)
        {
            List<Subtopic> data = mapper.Map<List<SubTopicModel>, List<Subtopic>>(lstSubTopicModel);
            foreach (Subtopic entity in data)
            {
                var subtopic = _subTopicRepository.Fetch(x => x.SubtopicId == entity.SubtopicId).SingleOrDefault();
                subtopic.SubtopicNo = entity.SubtopicNo;
                await _subTopicRepository.SaveChangesTaskAsync();
            }
            return "Sorted Topics Updated Successfully!";
        }

        public void DeleteSubTopicByTopicId(int topicId)
        {
            var record = _subTopicRepository.Fetch(x => x.TopicId == topicId).ToList();
            _subTopicRepository.DeleteAll(record);
            _subTopicRepository.SaveChanges();
        }
    }
}
