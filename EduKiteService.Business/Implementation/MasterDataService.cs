﻿using AutoMapper;
using EdukiteService.Business.Entities;
using EdukiteService.Data;
using EdukiteService.Data.interfaces;
using EdukiteService.Business.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EdukiteService.Business.Implementation
{
    public class MasterDataService :IMasterDataService
    {
        public IMapper Mapper { get; set; }
        private readonly IRepository<Country> _CountryRepository;
        public MasterDataService(IRepository<Country> CountryRepository)
        {
            _CountryRepository = CountryRepository;
        }
        public List<CountryModel> GetCountries()
        {
            var countries = _CountryRepository.GetAll();

            return Mapper.Map<List<Country>, List<CountryModel>>(countries.ToList());
        }
    }
}
