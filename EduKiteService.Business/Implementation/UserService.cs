﻿using EdukiteService.Data.interfaces;
using EdukiteService.Business.Entities;
using EdukiteService.Business.Interface;
using EdukiteService.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EdukiteService.Utilities;
using System.Data.SqlClient;
using System.Data;
using System.Data.Entity;

namespace EdukiteService.Business.Implementation
{
    public class UserService : IUserService
    {
        private readonly IRepository<User> _UserRepository;
        private readonly IRepository<UserType> _UserTypeRepository;
        private readonly IRepository<UserLogin> _UserLoginRepository;
        public UserService(IRepository<User> UserRepository, IRepository<UserType> UserTypeRepository, IRepository<UserLogin> UserLoginRepository)
        {
            _UserRepository = UserRepository;
            _UserTypeRepository = UserTypeRepository;
            _UserLoginRepository = UserLoginRepository;
        }
        public bool IsValidEmployee(UserModel userModel)
        {
            var employee = _UserRepository.Fetch(x => ((x.ContactNo != null && x.ContactNo.Equals(userModel.ContactNo)))).FirstOrDefault();

            var employeeDevice = _UserRepository.Fetch(x => (x.ContactNo == userModel.ContactNo)).FirstOrDefault();

            if (employee == null && employeeDevice == null)
                return false;

            return true;
        }
        public List<UserModel> GetEmployees(int UserId)
        {
            return _UserRepository.ExecWithStoreProcedure<UserModel>(Constants.SP_GETUSERS,
                 new SqlParameter(Constants.SP_PARAM_EMPLOYEEID, SqlDbType.Int) { Value = UserId } 
                 ).ToList();
        }
        public UserDetailsModel getUserDetails(int userId)
        {
            UserDetailsModel obj = _UserRepository.ExecWithStoreProcedure<UserDetailsModel>(Constants.SP_GETUSERDETAILS,
                  new SqlParameter(Constants.SP_PARAM_USERID, SqlDbType.Int) { Value = userId }
                  ).FirstOrDefault();

            var objgrades = _UserRepository.ExecWithStoreProcedure<Grades>(Constants.SP_GETUSERGRADES,
                new SqlParameter(Constants.SP_PARAM_USERID, SqlDbType.Int) { Value = userId }
                ).ToList();
            obj.grades = objgrades;
            return obj;
        }
        public bool UpdateUserDetails(UsersMode userDetailsModel)
        {
            var userModel = new User();
            userModel.LastName = userDetailsModel.userDetails.lastName;
            return true;
        }
    }
}
