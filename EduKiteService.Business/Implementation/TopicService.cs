﻿using AutoMapper;
using EdukiteService.Business.Entities;
using EdukiteService.Data;
using EdukiteService.Data.interfaces;
using EdukiteService.Utilities;
using EdukiteService.Business.Interface;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EdukiteService.Business.Implementation
{
    public class TopicService : ITopicService
    {
        public IMapper mapper { get; set; }
        public IRepository<TopicModel> _topicRepository;
        public IRepository<Topic> _topicRepositoryData;
        private readonly IAssetService _assetService;
        private readonly ISubTopicService _subTopicService;
        public TopicService(IRepository<TopicModel> topicRepository, IRepository<Topic> repository, IAssetService assetService, ISubTopicService subTopicService)
        {
            _topicRepository = topicRepository;
            _topicRepositoryData = repository;
            _assetService = assetService;
            _subTopicService = subTopicService;

        }
        public TopicDetailsModel GetTopicDetails(int topicId)
        {
            TopicDetailsModel obj = _topicRepository.ExecWithStoreProcedure<TopicDetailsModel>(Constants.SP_GETTOPICDETAILS,
                  new SqlParameter(Constants.SP_PARAM_ACTIVITY_TOPICID, SqlDbType.Int) { Value = topicId }
                  ).FirstOrDefault();

            List<SubTopicsDetailsModel> objsubTopics = _topicRepository.ExecWithStoreProcedure<SubTopicsDetailsModel>(Constants.SP_GETSUBTOPICDETAILSBYTOPIC,
                new SqlParameter(Constants.SP_PARAM_ACTIVITY_TOPICID, SqlDbType.Int) { Value = topicId }
                ).ToList();
            if (objsubTopics.Count != 0)
            {
                obj.subTopics = objsubTopics;
            }

            return obj;
        }

        public List<TopicModel> GetTopics()
        {
            var topics = _topicRepositoryData.GetAll().ToList();
            return mapper.Map<List<Topic>, List<TopicModel>>(topics);

            // return topics;
        }

        public List<TopicModel> GetTopicsBySubjcectID(int SubjID)
        {
            var topic = _topicRepositoryData.Fetch(x => x.SubjectId.Equals(SubjID)).OrderBy(x => x.TopicNo).ToList();
            return mapper.Map<List<Topic>, List<TopicModel>>(topic);
        }


        public TopicModel GetTopicByName(string topicName, int SubID)
        {
            var record = _topicRepositoryData.Fetch(x => x.TopicName == topicName && x.SubjectId == SubID).SingleOrDefault();
            TopicModel clientModel = mapper.Map<Topic, TopicModel>(record);
            return clientModel;
        }


        public void AddTopic(TopicModel topicModel)
        {
            int topicNo = _topicRepositoryData.Fetch(x => x.SubjectId == topicModel.SubjectId).Count();

            if (topicNo >= 0)
            {
                var topicNotoInsert = 1 + topicNo;
                topicModel.TopicNo = topicNotoInsert;
            }

            Topic clientModel = mapper.Map<TopicModel, Topic>(topicModel);
            _topicRepositoryData.Add(clientModel);
            _topicRepositoryData.SaveChanges();
        }

        public TopicModel GetTopicById(int topicId)
        {
            var record = _topicRepositoryData.Fetch(x => x.TopicId == topicId).SingleOrDefault();
            TopicModel clientModel = mapper.Map<Topic, TopicModel>(record);
            return clientModel;
        }

        public void UpdateTopic(TopicModel topicModel)
        {
            Topic clientModel = mapper.Map<TopicModel, Topic>(topicModel);
            _topicRepositoryData.UpdateChanges(clientModel);
        }

        public void DeleteTopic(int topicID)
        {
            _assetService.DeleteAssetByTopicId(topicID);
            _subTopicService.DeleteSubTopicByTopicId(topicID);
            Topic record = _topicRepositoryData.Fetch(x => x.TopicId == topicID).SingleOrDefault();
            _topicRepositoryData.Delete(record);
            _topicRepositoryData.SaveChanges();
        }

        public int AddTopicRetID(TopicModel topicModel)
        {
            var record = _topicRepositoryData.Fetch(x => x.TopicName == topicModel.TopicName && x.SubjectId == topicModel.SubjectId).SingleOrDefault();
            TopicModel clientModel = mapper.Map<Topic, TopicModel>(record);
            if (clientModel != null && clientModel.TopicId != 0)
            {
                return clientModel.TopicId;
            }
            else
            {
                Topic clientModelAdd = mapper.Map<TopicModel, Topic>(topicModel);
                _topicRepositoryData.Add(clientModelAdd);
                _topicRepositoryData.SaveChanges();
                return clientModelAdd.TopicId;
            }
        }

        public async Task<string> UpdateSortedTopics(List<TopicModel> lsttopicModel)
        {
            List<Topic> data = mapper.Map<List<TopicModel>, List<Topic>>(lsttopicModel);
            foreach (Topic entity in data)
            {
                var topic = _topicRepositoryData.Fetch(x => x.TopicId == entity.TopicId).SingleOrDefault();
                topic.TopicNo = entity.TopicNo;
                await _topicRepositoryData.SaveChangesTaskAsync();
            }
            return "Sorted Topics Updated Successfully!";
        }

        public void DeleteTopicBySubjectId(int SubjID)
        {
            var record = _topicRepositoryData.Fetch(x => x.SubjectId == SubjID).ToList();
            if (record != null)
            {
                for (int i = 0; i < record.Count(); i++)
                {
                    _assetService.DeleteAssetByTopicId(record[i].TopicId);
                    _subTopicService.DeleteSubTopicByTopicId(record[i].TopicId);
                }
            }
            _topicRepositoryData.DeleteAll(record);
            _topicRepositoryData.SaveChanges();
        }
    }
}
