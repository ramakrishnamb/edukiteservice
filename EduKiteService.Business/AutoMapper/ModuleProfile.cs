﻿using AutoMapper;
using EdukiteService.Business.Entities;
using EdukiteService.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EduKiteService.Business.AutoMapper
{
    public class ModuleProfile : Profile
    {
        public ModuleProfile()
        {
            
            CreateMap<ModuleModel, Module>();
            CreateMap<Module, ModuleModel>();
        }
    }
}
