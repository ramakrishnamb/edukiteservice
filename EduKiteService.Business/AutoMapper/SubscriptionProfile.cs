﻿using AutoMapper;
using EdukiteService.Business.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EdukiteService.Business.AutoMapper
{
    public class SubscriptionProfile : Profile
    {
        public SubscriptionProfile()
        {
            CreateMap<IEnumerable<SubscriptionsModel>, SubscriptionResponse>().ForMember(dest => dest.Subscriptions, opt => opt.MapFrom(src => src));
        }
    }
}
