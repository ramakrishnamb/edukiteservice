﻿using EdukiteService.Business.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EdukiteService.Business.Interface
{
    public interface ISubTopicService
    {
        List<SubTopicModel> GetSubTopics(int topicId);
        void AddSubTopics(SubTopicModel stData);
        SubTopicModel GetSubTopicByName(string SubtopicName);
        SubTopicModel GetSubTopicById(int SubtopicId);
        void UpdateSubTopics(SubTopicModel stData);
        void DeleteSubTopic(int SubtopicId);
        int AddSubTopicRetID(SubTopicModel subTopicModel);

        Task<string> UpdateSortedSubTopics(List<SubTopicModel> lstSubTopicModel);
        void DeleteSubTopicByTopicId(int topicId);
    }
}
